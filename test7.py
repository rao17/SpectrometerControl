import time
import serial

# configure the serial connections (the parameters differs on the device you are connecting to)
ser = serial.Serial(port='COM4', baudrate=19200, bytesize=8, parity='N',
                    stopbits=1, timeout=1)

ser.isOpen()

print('Enter your commands below.\r\nInsert "exit" to leave the application.')

# input=1
while 1:
    # get keyboard input
    x = input(">> ")
    if x == 'exit':
        ser.close()
        exit()
    else:
        # send the character to the device
        # (note that I happened a \r\n carriage return and line feed to the characters - this is requested by my device)
        x = x + '\r\n'
        byteConverted = x.encode()
        ser.write(byteConverted)
        out = ''
        # let's wait one second before reading output (let's give device time to answer)
        time.sleep(1)
        while ser.inWaiting() > 0:
            temp = ser.read(1)
            outTemp = temp.decode("utf-8")
            out = out + outTemp

        if out != '':
            print(">>"+ out)