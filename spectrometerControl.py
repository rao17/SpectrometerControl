import seabreeze.spectrometers as sb
import numpy as np
import pyqtgraph as pg
from pyqtgraph.Qt import QtGui, QtCore
from pyqtgraph.ptime import time
app = QtGui.QApplication([])

p = pg.plot()
# p.setWindowTitle('pyqtgraph example: PlotSpeedTest')
# p.setRange(QtCore.QRectF(0, -10, 5000, 20)) 
# p.setLabel('bottom', 'Index', units='B')
curve = p.plot()

devices = sb.list_devices()
if devices == []:
    print("No devices found")
    exit()

spec = sb.Spectrometer(devices[0])
spec.integration_time_micros(100000)

def update():
    global curve
    wavelengths = np.array(spec.wavelengths())
    intensities = np.array(spec.intensities())
    curve.setData(wavelengths,intensities)
    app.processEvents()  # force complete redraw for every plot

timer = QtCore.QTimer()
timer.timeout.connect(update)
timer.start(0)
    


## Start Qt event loop unless running in interactive mode.
if __name__ == '__main__':
    import sys
    if (sys.flags.interactive != 1) or not hasattr(QtCore, 'PYQT_VERSION'):
        QtGui.QApplication.instance().exec_()







