import serial as ser

laserSerial = ser.Serial(port='COM4', baudrate=19200, bytesize=8, parity='N',
                         stopbits=1, timeout=1)
print(laserSerial.name)
laserSerial.write(b'?H')
laserSerial.read()